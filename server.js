"use strict";

const express = require("express");
const morgan = require("morgan");
const app = express();
const PORT = 8081;

app.use(morgan("combined"));

app.use(express.static("public"));

app.get("*", (req, res) => {
  res.status(404).send("Not Found 🐸");
});

app.listen(PORT, () => {
  console.log(`Server listening at http://127.0.0.1:${PORT}`);
});